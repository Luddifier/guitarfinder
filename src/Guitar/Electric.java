package Guitar;

public class Electric extends Guitar {
    private int pickups;

    public Electric(String brand, String model, int year, int pickups){
        super(brand, model, year);
        this.pickups = pickups;
    }

    @Override
    public void makeSound(){
        System.out.println("The electric guitar that is a " + this.getBrand() + " of model " + this.getModel() + " made in the year " + this.getYear() + " makes an electrifyingly awesome sound!!!!!");
    }







}

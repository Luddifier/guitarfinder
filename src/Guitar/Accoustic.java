package Guitar;

public class Accoustic extends Guitar {

    public Accoustic(String brand, String model, int year){
        super(brand, model, year);

    }
    @Override
    public void makeSound() {
        System.out.println("The accoustic guitar that is a " + this.getBrand() + " of model " + this.getModel() + " made in the year " + this.getYear() + " makes a wholly soothing and satisfying impression.");

    }
}

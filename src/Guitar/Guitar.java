package Guitar;

public abstract class Guitar implements makeSound {
    private String brand;
    private String model;
    private int year;

    public Guitar(String brand, String model, int year) {
        this.brand = brand;
        this.model = model;
        this.year = year;
     }

    public String getBrand() {
        return brand;
    }

    public int getYear() {
        return year;
    }

    String getModel() {
        return model;
    }
    public abstract void makeSound();
}

import Guitar.*;

import java.util.ArrayList;

public class Main {
    public static void main(String[] args) {

        Guitar badassGuitar = new Electric("Gibson", "Badass", 1973, 3);
        Guitar classicGuitar = new Accoustic("Cornelis", "Venerable", 1951);

        ArrayList<Guitar> listOfGuitars = new ArrayList<Guitar>();

        listOfGuitars.add(badassGuitar);
        listOfGuitars.add(classicGuitar);

        for (Guitar currentGuitar:listOfGuitars){
            currentGuitar.makeSound();
        }

    }
}
